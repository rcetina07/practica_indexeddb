# README - Rodolfo Rafael Dzul Cetina #

Ejercicio IndexedDB

### caracterÝsticas ###

* Version 1.1

### Assets ###

* Jquery
* Bootstrap
* IziModal
* IziToast

### Capturas ###

![](https://screenshots.firefoxusercontent.com/images/5bd68242-a5b5-4aef-8d9c-22ed4cb7ac04.png)

![](https://screenshots.firefoxusercontent.com/images/21edb6f6-b983-4465-ad98-d6af9eac8638.png)

![](https://screenshots.firefoxusercontent.com/images/25814459-227b-49fd-a698-42b56473035b.png)

![](https://screenshots.firefoxusercontent.com/images/582988ef-705b-4b68-b24c-64e5c347700e.png)
